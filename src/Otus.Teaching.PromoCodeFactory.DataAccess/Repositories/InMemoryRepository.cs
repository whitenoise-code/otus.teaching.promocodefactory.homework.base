﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {

        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Guid Add(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data.Add(entity);
            return entity.Id;
        }

        public Guid Update(T entity)
        {
            var ind = Data.FindIndex(item => item.Id == entity.Id);
            Data[ind] = entity;
            return entity.Id;

        }

        public void Delete(Guid id)
        {
            var item = Data.First(entity => entity.Id == id);
            Data.Remove(item);
        }
    }
}