﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class EmployeeExtensions
    {
        public static EmployeeResponse ToResponse(this Employee emp)
        {

            return new EmployeeResponse
            {
                Id = emp.Id,
                Email = emp.Email,
                FullName = emp.FullName,
                Roles = emp.Roles.Select(r => new RoleItemResponse
                {
                    Id = r.Id,
                    Description = r.Description,
                    Name = r.Name
                }).ToList()
            };
        }


        public static Employee ToEmployee(this EmployeeRequest emp)
        {

            return new Employee
            {
                Id = emp.Id,
                Email = emp.Email,
                FirstName = emp.FullName.Split(" ").First(),
                LastName = emp.FullName.Split(" ").Last(),
                Roles = emp.Roles.Select(r => new Role
                {
                    Id = r.Id,
                    Description = r.Description,
                    Name = r.Name
                }).ToList(),
                AppliedPromocodesCount = emp.AppliedPromocodesCount
            };
        }

    }
}
